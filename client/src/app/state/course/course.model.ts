import { guid, ID } from '@datorama/akita';

export interface Courses {
  id: ID;
  count: number;
  next: string;
  previous: string;
  results: Course[];
}

// Course is the structure of udemy course response
export interface Course {
  _class: string;
  id: number;
  title: string;
  url: string;
  is_paid: boolean;
  price: string;
  price_detail: PriceDetail;
  visible_instructors: VisibleInstructors;
  image_125_H: string;
  image_240x135: string;
  is_practice_test_course: boolean;
  image_480x270: string;
  published_title: string;
}

// PriceDetail is the structure of course.price_detail
export interface PriceDetail {
  amount: number;
  currency: string;
  price_string: string;
  currency_symbol: string;
}

// VisibleInstructors is the structure of course.visible_instructors
export interface VisibleInstructors {
  _class: string;
  id: number;
  title: string;
  name: string;
  display_name: string;
  job_title: string;
  image_50x50: string;
  image_100x100: string;
  initials: string;
  url: string;
}

/**
 * A factory function that creates Courses
 */
export function createCourses({
  count,
  next,
  previous,
  results,
}: Partial<Courses>): Courses {
  return {
    id: guid(),
    count,
    next,
    previous,
    results,
  };
}
