import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Courses } from './course.model';

export interface CourseState extends EntityState<Courses> {}


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'course' })
export class CourseStore extends EntityStore<CourseState, Courses> {

  constructor() {
    super();
  }
}
