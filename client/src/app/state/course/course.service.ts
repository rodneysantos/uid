import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { Courses } from './course.model';
import { CourseStore } from './course.store';

@Injectable({ providedIn: 'root' })
export class CourseService {

  /**
   * Creates an instance of CourseService.
   * @param {CourseStore} courseStore
   * @param {HttpClient} http
   * @memberof CourseService
   */
  constructor(
    private courseStore: CourseStore,
    private http: HttpClient
  ) { }

  get() {
    this.http.get('http://localhost:4000/courses').subscribe(entities => this.courseStore.set(entities));
  }

  add(courses: Courses) {
    this.courseStore.add(courses);
  }

  update(id, courses: Partial<Courses>) {
    this.courseStore.update(id, courses);
  }

  remove(id: ID) {
    this.courseStore.remove(id);
  }
}
