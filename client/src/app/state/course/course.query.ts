import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { Courses } from './course.model';
import { CourseState, CourseStore } from './course.store';

@Injectable({
  providedIn: 'root'
})
export class CourseQuery extends QueryEntity<CourseState, Courses> {

  constructor(protected store: CourseStore) {
    super(store);
  }
}
