import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Courses } from './../../state/course/course.model';
import { CourseQuery } from './../../state/course/course.query';
import { CourseService } from './../../state/course/course.service';

@Component({
  selector: 'ud-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss'],
})
export class CourseListComponent implements OnInit {

  /**
   * courses$ is an observable containing Courses object
   *
   * @type {Observable<Courses>}
   * @memberof CourseListComponent
   */
  courses$: Observable<Courses>;

  /**
   * Creates an instance of CourseListComponent.
   * @param {CourseService} courseService
   * @param {CourseQuery} courseQuery
   * @memberof CourseListComponent
   */
  constructor(
    private courseService: CourseService,
    private courseQuery: CourseQuery,
  ) { }

  /**
   * ngOnInit - Angular lifecycle
   * @memberof CourseListComponent
   */
  ngOnInit() {
    this.courses$ = this.courseQuery.select();
    this.courseService.get();
  }
}
