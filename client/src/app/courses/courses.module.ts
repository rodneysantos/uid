import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material';
import { MaterialModule } from './../material/material.module';
import { SharedModule } from './../shared/shared.module';
import { CourseListComponent } from './course-list/course-list.component';
import { CoursesRoutingModule } from './courses-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MatCardModule,
    MaterialModule,
    SharedModule,
    CoursesRoutingModule,
  ],
  declarations: [
    CourseListComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class CoursesModule { }
