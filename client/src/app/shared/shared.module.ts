import { NgModule } from '@angular/core';
import { LimitTextPipe } from './pipes/limit-text.pipe';

@NgModule({
  declarations: [
    LimitTextPipe,
  ],
  exports: [
    LimitTextPipe,
  ],
})
export class SharedModule { }
