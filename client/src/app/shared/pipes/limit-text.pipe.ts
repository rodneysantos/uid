import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitText'
})
export class LimitTextPipe implements PipeTransform {

  transform(text: string, limit: number): string {
    if (text.length <= limit) {
      return text;
    }

    const trimmed = text.split('').splice(0, limit - 1).join('');
    return `${trimmed.trim()}...`;
  }
}
