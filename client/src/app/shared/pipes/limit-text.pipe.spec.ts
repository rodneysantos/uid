import { LimitTextPipe } from './limit-text.pipe';

describe('LimitTextPipe', () => {
  it('should limit the given text', () => {
    const pipe = new LimitTextPipe();
    const trimmed = pipe.transform(`Jose Portilla's Pierian Data Inc. International Translations`, 50);
    expect(trimmed).toEqual(`Jose Portilla's Pierian Data Inc. International T...`);

    const untrimmed = pipe.transform(`Data Scientist`, 50);
    expect(untrimmed).toEqual(`Data Scientist`);
  });
});
