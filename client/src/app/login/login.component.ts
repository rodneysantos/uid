import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ud-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /**
   * form holds the email and password controls
   *
   * @type {FormGroup}
   * @memberof LoginComponent
   */
  form: FormGroup;

  /**
   * Creates an instance of LoginComponent.
   * @param {FormBuilder} fb
   * @memberof LoginComponent
   */
  constructor(private fb: FormBuilder) { }

  /**
   * ngOnInit - Angular lifecycle
   * @memberof LoginComponent
   */
  ngOnInit() {
    this.form = this.formSetup();
  }

  private formSetup(): FormGroup {
    return this.fb.group({
      email: this.fb.control('', [Validators.required]),
      password: this.fb.control('', [Validators.required])
    });
  }
}
