import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../material/material.module';
import { LoginRoutingModule } from './login-routing.modue';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    MaterialModule,
  ],
  declarations: [
    LoginComponent,
  ],
})
export class LoginModule { }
