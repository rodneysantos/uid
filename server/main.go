package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/rodneysantos/uid/internal/pkg/auth"
	"github.com/rodneysantos/uid/internal/pkg/udemy"
)

func main() {
	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
    AllowOrigins: []string{"http://localhost:4200"},
    AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
  }))

	e.GET("/course", udemy.SubscribedCourse)
	e.GET("/courses", udemy.SubscribedCourses)
	e.POST("/login", auth.Login)
	e.Logger.Fatal(e.Start(":4000"))
}
