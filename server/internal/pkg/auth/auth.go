package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo"
)

// User credentials
type User struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Udemy api response
type Udemy struct {
	Class       string `json:"_class"`
	ID          int    `json:"ID"`
	AccessToken string `json:"access_token"`
}

// Login calls the Udemy api along with user's credentials
// to get an access_token
func Login(c echo.Context) error {
	u := User{
		Email:    c.FormValue("email"),
		Password: c.FormValue("password"),
	}

	creds, err := json.Marshal(u)
	if err != nil {
		return err
	}

	udemy, err := udemyRequest(creds)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, udemy)
}

func udemyRequest(creds []byte) (*Udemy, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("POST", "https://www.udemy.com/api-2.0/auth/udemy-auth/login/?fields[user]=access_token", bytes.NewBuffer(creds))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic YWQxMmVjYTljYmUxN2FmYWM2MjU5ZmU1ZDk4NDcxYTY6YTdjNjMwNjQ2MzA4ODI0YjIzMDFmZGI2MGVjZmQ4YTA5NDdlODJkNQ==")
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var udemy Udemy
	err = json.Unmarshal(body, &udemy)
	if err != nil {
		return nil, err
	}

	return &udemy, nil
}
