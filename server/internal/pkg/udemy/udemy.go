package udemy

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/labstack/echo"
)

// Courses is the structure of udemy courses response
type Courses struct {
	Count    uint8    `json:"count"`
	Next     string   `json:"next"`
	Previous string   `json:"previous"`
	Results  []Course `json:"results"`
}

// Course is the structure of udemy course response
type Course struct {
	Class                string               `json:"_class"`
	ID                   uint32               `json:"id"`
	Title                string               `json:"title"`
	URL                  string               `json:"url"`
	IsPaid               bool                 `json:"is_paid"`
	Price                string               `json:"price"`
	PriceDetail          PriceDetail          `json:"price_detail"`
	VisibleInstructors   []VisibleInstructors `json:"visible_instructors"`
	Image125H            string               `json:"image_125_H"`
	Image240135          string               `json:"image_240x135"`
	IsPracticeTestCourse bool                 `json:"is_practice_test_course"`
	Image480270          string               `json:"image_480x270"`
	PublishedTitle       string               `json:"published_title"`
}

// PriceDetail is the structure of course.price_detail
type PriceDetail struct {
	Amount         float32 `json:"amount"`
	Currency       string  `json:"currency"`
	PriceString    string  `json:"price_string"`
	CurrencySymbol string  `json:"currency_symbol"`
}

// VisibleInstructors is the structure of course.visible_instructors
type VisibleInstructors struct {
	Class       string `json:"_class"`
	ID          uint32 `json:"id"`
	Title       string `json:"title"`
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	JobTitle    string `json:"job_title"`
	Image5050   string `json:"image_50x50"`
	Image100100 string `json:"image_100x100"`
	Initials    string `json:"initials"`
	URL         string `json:"url"`
}

// SubscribedCourses returns all user's subscribed courses
func SubscribedCourses(c echo.Context) error {
	var courses Courses

	resBody, err := udemyRequest("http://localhost:4100/courses")
	if err != nil {
		return err
	}

	err = json.Unmarshal(resBody, &courses)
	if err != nil {
		log.Println(err)
		return err
	}

	return c.JSON(http.StatusOK, courses)
}

// SubscribedCourse returns a single course
func SubscribedCourse(c echo.Context) error {
	var course Course

	resBody, err := udemyRequest("http://localhost:4100/course")
	if err != nil {
		return err
	}

	err = json.Unmarshal(resBody, &course)
	if err != nil {
		log.Println(err)
		return err
	}

	return c.JSON(http.StatusOK, course)
}

func udemyRequest(uri string) ([]byte, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Basic YWQxMmVjYTljYmUxN2FmYWM2MjU5ZmU1ZDk4NDcxYTY6YTdjNjMwNjQ2MzA4ODI0YjIzMDFmZGI2MGVjZmQ4YTA5NDdlODJkNQ==")
	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return body, nil
}
