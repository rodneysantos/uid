package storage

import (
	"github.com/dgraph-io/badger"
)

// DB wraps the badger database
type DB struct {
	b *badger.DB
}

// New connects to badger database
func New(dir string) *DB {
	storage := &DB{}
	opts := badger.DefaultOptions
	opts.Dir = dir
	opts.ValueDir = dir

	var err error
	storage.b, err = badger.Open(opts)
	if err != nil {
		panic(err)
	}

	return storage
}

// Close closes badger connection
func (db *DB) Close() error {
	err := db.b.Close()
	if err != nil {
		return err
	}

	return nil
}

// Set implements db.Set
func (db *DB) Set(key []byte, value []byte) error {
	err := db.b.Update(func(txn *badger.Txn) error {
		return txn.Set(key, value)
	})
	if err != nil {
		return err
	}

	return nil
}

// Get implements db.Get
func (db *DB) Get(key []byte) ([]byte, error) {
	var value []byte
	err := db.b.View(func(txn *badger.Txn) error {
		item, err := txn.Get(key)
		if err != nil {
			return err
		}

		value, err = item.ValueCopy(value)
		if err != nil {
			return err
		}

		return nil
	})

	return value, err
}

// Delete implements db.delete
func (db *DB) Delete(key []byte) error {
	txn := db.b.NewTransaction(true)
	defer txn.Discard()

	return txn.Delete(key)
}
