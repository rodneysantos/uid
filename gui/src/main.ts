import { app, BrowserWindow, screen, shell } from 'electron';
import * as reload from 'electron-reload';

let win: BrowserWindow;

function createWindow() {
  const size = screen.getPrimaryDisplay().workAreaSize;
  win = new BrowserWindow({
    x: 0,
    y: 0,
    height: size.height,
    width: size.width,
  });

  reload(`${__dirname}/../../client`, { electron: require(`${__dirname}/../node_modules/electron`) });
  win.loadURL('http://localhost:4200');

  win.on('closed', () => {
    win = null;
  });

  startServer();
}

function startServer() {
  // @todo: terminal should not be visible
  shell.openItem('D:\\Workspace\\uid\\gui\\dist\\server.exe');
}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  app.on('activate', startServer);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}
